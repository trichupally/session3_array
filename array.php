<?php
$arr = array ("Google",
    "Yahoo",
    'Bing',
    array('Search_Engine' => 'Chagol',
        'New',
        "pondit" => "PHP",
        array('Search_Engine' => 'Chagol',
            'New',
            "pondit" => "THP")
    )
);

echo "<pre>";
echo $arr[2];

echo "</br>";

echo $arr[3]; /* output will be error, cause 3rd element is a array, and array can't be
 defined by echo*/

echo "</br>";

print_r($arr); // to see array o/p

echo "</br>";

print_r($arr [3]); // to see 3rd element of main array

echo "</br>";

print_r($arr [3][1]); // to see 3rd element's (nested array) 1st element of the array

echo "</br>";

print_r($arr [3][1]['pondit']);

?>